#/bin/sh

# Desktop
pacman -Sy i3-gaps 
pacman -Sy i3lock 
pacman -Sy dmenu
pacman -Sy xorg-server xorg-xinit

# Utilities
pacman -Sy sudo 
pacman -Sy firefox 
