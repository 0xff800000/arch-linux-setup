#/bin/sh

sudo pacman -Sy make
sudo pacman -Sy vim
sudo pacman -Sy zsh 
sudo pacman -Sy ranger
sudo pacman -Sy htop
sudo pacman -Sy screenfetch
sudo pacman -Sy openssh
sudo pacman -Sy tmux
sudo pacman -Sy fzf
