#/bin/sh

# Build and install polybar
sudo pacman -Sy cmake 
mkdir ~/setup
cd ~/setup
git clone https://aur.archlinux.org/polybar.git
cd polybar
makepkg
pacman -U *.tar.xz

# install dotfiles
cd ~/setup
git clone https://github.com/0xff800000/dotfiles.git
cd dotfiles
make i3_link
make polybar_link
make tmux_link
make vim_init
make vim_link
make zsh_link
source ~/.zshrc

sudo pacman -Sy powerline-fonts
sudo pacman -Sy feh 
sudo pacman -Sy scrot 
sudo pacman -Sy imagemagick 

# Polybar dependencies
sudo pacman -Sy ttf-dejavu
sudo pacman -Sy bdf-unifont
cd ~/setup
git clone https://aur.archlinux.org/siji-git.git
cd siji-git
makepkg
sudo pacman -U *.tar.*
