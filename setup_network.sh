#!/bin/sh

# Enable interface
ip link set eno1 up
dhcpcd eno1
ping -c 4 1.1.1.1

# Install network manager
pacman -Sy networkmanager
systemctl enable networkmanager
systemctl restart networkmanager


